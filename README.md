# Projeto prático do useReducer

## Instale as dependências
`npm install` ou `yarn install`

## API 
`https://jsonplaceholder.typicode.com/posts`

## App
`npm run dev` ou `yarn dev`

# Projeto final

<img src="./src/assets/fetch-use-reducer.png" />