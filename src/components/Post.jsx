import { useReducer, useState } from 'react'
import { ACTION_TYPES } from '../Reducers/postActionType'
import { INITIAL_STATE, postReducer } from '../Reducers/postReducer'
import './Post.css'

const Post = () => {
  const [state, dispatch] = useReducer(postReducer, INITIAL_STATE)

  const handleFetch = () => {
    dispatch({ type: ACTION_TYPES.FETCH_START })

    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(res => res.json())
      .then(data => dispatch({ type: ACTION_TYPES.FETCH_SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: ACTION_TYPES.FETCH_ERROR }))
  }

  return (
    <div>
      <button type='button' onClick={handleFetch}>
        {state.loading ? 'Carregando' : 'Fetch posts'}
      </button>
      {
        state.post.length >= 0 ?
        state.post.map(item => (
          <div key={item.id} style={{ maxWidth: '600px' }}>
            <h2>{item.title}</h2>
            <p >{item.body}</p>
          </div>
          
        ))
        : ''
      }

      <span>{state.error && 'Something went wrong!'}</span>
    </div>
  )
}

export default Post